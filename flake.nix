{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs?ref=22.11";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
      in rec {
        packages = import ./default.nix { inherit pkgs; };
        apps.xls-to-csv = flake-utils.lib.mkApp { drv = packages.ut3_survival; exePath = "/bin/xls-to-csv"; };
        defaultPackage = packages.ut3_survival;
      }
    );
}
